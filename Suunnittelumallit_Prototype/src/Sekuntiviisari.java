
public class Sekuntiviisari implements Cloneable{
	
	private int sekunti;
	
	public Sekuntiviisari(int sekunti) {
		this.sekunti = sekunti;
	}
	
	public int getSekunti() {
		return this.sekunti;
	}
	
	public void setSekunti(int sekunti) {
		this.sekunti = sekunti;
	}
	
	public Object clone() {
		try {
			return super.clone();
		}catch(CloneNotSupportedException e) {
			System.out.println(e);
			return null;
		}
	}

}
