
public class Minuuttiviisari implements Cloneable {
	
	private int minuutti;
	
	public Minuuttiviisari(int minuutti) {
		this.minuutti = minuutti;
	}
	
	public int getMinuutti() {
		return this.minuutti;
	}
	
	public void setMinuutti(int minuutti) {
		this.minuutti = minuutti;
	}
	
	public Object clone() {
		try {
			return super.clone();
		}catch(CloneNotSupportedException e) {
			System.out.println(e);
			return null;
		}
	}

}
