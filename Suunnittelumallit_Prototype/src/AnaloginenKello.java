
public class AnaloginenKello implements Cloneable {
	
	private Tuntiviisari tunti;
	private Minuuttiviisari minuutti;
	private Sekuntiviisari sekunti;
	
	public AnaloginenKello(int tunti, int minuutti, int sekunti) {
		this.tunti = new Tuntiviisari(tunti);
		this.minuutti = new Minuuttiviisari(minuutti);
		this.sekunti = new Sekuntiviisari(sekunti);
	}

	
	public Object clone() {
		try {
			return super.clone();
		}catch(CloneNotSupportedException e) {
			System.out.println(e);
			return null;
		}

	}
	
	public Tuntiviisari getTunti() {
		return tunti;
	}
	
	public Minuuttiviisari getMinuutti() {
		return minuutti;
	}
	
	public Sekuntiviisari getSekunti() {
		return sekunti;
	}
	
	/*public AnaloginenKello clone() {
		AnaloginenKello kello = null;
		try {
			kello = (AnaloginenKello) super.clone();
			kello.tunti = (Tuntiviisari) tunti.clone();
			kello.minuutti = (Minuuttiviisari) minuutti.clone();
			kello.sekunti = (Sekuntiviisari) sekunti.clone();
		}catch(CloneNotSupportedException e) {
			System.out.println(e);
		}
		return kello;
		
	}*/

	public String naytaAika() {
		return tunti.getTunti() + ":" + minuutti.getMinuutti() + ":" + sekunti.getSekunti();
	}

}
