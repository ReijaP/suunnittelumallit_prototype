
public class Main {

	public static void main(String[] args) {
		
		AnaloginenKello kello = new AnaloginenKello(13,45,17);
		
		System.out.println(kello.naytaAika());
		
		AnaloginenKello kello2 = (AnaloginenKello) kello.clone();
		
		System.out.println(kello2.naytaAika());
		
		kello2.getTunti().setTunti(11);
		kello2.getMinuutti().setMinuutti(11);
		kello2.getSekunti().setSekunti(11);
		
		System.out.println(kello.naytaAika());
		System.out.println(kello2.naytaAika());

	}

}
