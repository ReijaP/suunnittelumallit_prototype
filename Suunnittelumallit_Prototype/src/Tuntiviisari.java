
public class Tuntiviisari implements Cloneable{
	
	private int tunti;
	
	public Tuntiviisari(int tunti) {
		this.tunti = tunti;
	}
	
	public int getTunti() {
		return this.tunti;
	}
	
	public void setTunti(int tunti) {
		this.tunti = tunti;
	}
	
	public Object clone() {
		try {
			return super.clone();
		}catch(CloneNotSupportedException e) {
			System.out.println(e);
			return null;
		}
	}

}
